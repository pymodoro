from setuptools import setup, find_packages
from codecs import open
from import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name = 'pymodoro'
    version = '0.0.1',
    description = 'A command line pomodoro timer'
    long_description = long_description,
    url='https://github.com/noaoh/pomodoro-timer',
    author='Noah Holt'
    author_email= 'noahryanholt@tutanota.de'
    license='GPL3'
    classifiers=[
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Developers who want to be productive'
        'Topic :: Productivity',
        'License :: OSI Approved',
        'Programming Language :: Python :: 3'
    ],
    keywords = 'productivity, pomodoro, timer'
    packages = find_packages('time, pyglet, gi')
    install_requires = [
        'time',
        'pyglet',
        'gi'
    ]
    extras_require={
    },
    package_data={
    },
    data_files=[]
    entry_points={}
)



    

        
