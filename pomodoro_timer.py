from time import sleep
from pyglet import media
from gi import require_version
require_version('Notify', '0.7')
from gi.repository import Notify

def pomodoro_timer(work_time):
    short_break = work_time * .2 
    long_break = work_time * .6

    player = media.Player()
    alarm = media.StaticSource(media.load('analog-alarm-clock.wav'))
    player.play()

    pomodoro_short_break_note = Notify.Notification.new("Time's up, take a break!")
    pomodoro_work_note = Notify.Notification.new("It's work time!")
    pomodoro_long_break_note = Notify.Notification.new("Hope you got everything done, time's up!")
    Notify.init("Hello world!")

    x = 0 
    
    while x < 4:
        x += 1
        sleep(work_time * 60)
        player.queue(alarm)
        pomodoro_short_break_note.show()
        sleep(short_break * 60)
        player.queue(alarm)
        pomodoro_work_note.show()

    player.queue(alarm)
    pomodoro_long_break_note.show()
    sleep(long_break * 60)

if __name__ == "__main__":
    import sys
    pomodoro_timer(int(sys.argv[1]))
